package br.upis;

public class Horario {

		private byte hora; 		// {0 .. 23}
		private byte minuto; 	// {0 .. 59}
		private byte segundo; 	// {0 .. 59}
		
		public Horario(byte hora, byte minuto, byte segundo){
			setHora(hora);
			setMinuto(minuto);
			setSegundo(segundo);
		}
		
		public void setHora(byte hora) {
			
			if(hora >= 0 && hora <= 23) {
				this.hora = hora;
			}
		}
		
		public byte getHora() {
			return this.hora;
		}		
	
		public void setMinuto(byte minuto) {
			if(minuto >= 0 && minuto <= 59) {
				this.minuto = minuto;
			}
		}
		
		public byte getMinuto() {
			return this.minuto;
		}
		
		public void setSegundo(byte segundo) {
			if(segundo >= 0 && segundo <= 59) {
				this.segundo = segundo;
			}
		}
		
		public byte getSegundo() {
			return this.segundo;
		}
		
		public String toString() {
			return getHora() + ":" + getMinuto() + ":" + getSegundo();
		}	
}
